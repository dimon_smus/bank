class ApplicationMailer < ActionMailer::Base
  default from: "dmitriy.smus@gmail.com"
  layout 'mailer'
end
