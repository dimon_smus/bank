class UserMailer < ApplicationMailer

  # Check user with negative balance and notificate them
  def self.notification_email
    @user = User.all

    @user.each do |current|
      mail(to: current.email, subject: 'Sloboda Shared Bank Notification')
      if (current.balance_give - current.balance_take) < 0
        mail(to: current.email, subject: 'Sloboda Shared Bank Notification')
      end
    end
  end
end
