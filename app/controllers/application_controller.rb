class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_action :check_admin

  private

  def check_admin

    if !current_user.nil? && !current_user.admin?

      redirect_to new_user_session_url, :flash => { :access_error => "Only Admin can give you access" }
    end
  end
end
