class UsersController < ApplicationController

  skip_before_action :authenticate_user!, only: :index
  before_action :show_user, only: [:show, :edit]
  skip_before_action :check_admin, only: [:list, :invite, :create_invite]

  PASSWORD_LENGTH = 10

  ##
  # Show current bank balance
  # #
  def index
    if @bank_balance = SharedBank.last
      @bank_balance = @bank_balance.current.to_i
    else
      #Empty bank balance
      @bank_balance = 0
    end
  end

  ##
  # Show users list
  # #
  def list
    @users = User.all
  end

  ##
  # Admin part
  # Create invite to user
  # GET @email
  # #

  def invite
    @user = User.new
  end

  def create_invite

    email = name = params[:user][:email]
    password = Devise.friendly_token.first(PASSWORD_LENGTH)

    if User.find_by(:email => email).nil?
      @user = User.create!({ :name => name,
                             :email => email,
                             :password => password,
                             :password_confirmation => password })
    else
      #Already registered
    end

    redirect_to root_path
  end

  ##
  # Only for current user
  # Show user profile
  # Show user transaction
  # Get @current_user
  # #

  def show
    @user = User.find(params[:id])
    @transactions =  User.find(current_user.id)
                         .transactions
                         .order(created_at: :desc)
                         .paginate(:page => params[:page], :per_page => 5)

    @given = User.balance_give params[:id]
    @taken = User.balance_take params[:id]
  end

  def edit
    @user = User.find(params[:id])
  end

  def save_profile

    user = User.find(current_user.id)

    user.update_attributes(user_params)

    redirect_to root_path
  end

  ##
  # Only for current user
  # Create positive transaction
  # Create negative transaction
  # Get @current_user
  # #

  def take
    @transaction = Transaction.new
  end

  def give
    @transaction = Transaction.new
  end

  def save_transaction

    if SharedBank.update_balance(params[:transaction])
      @transaction = Transaction.create(transaction_params)
    else
      flash[:access_error] = "There are not enough money in Shared Bank, plese choose another balance"
    end

    redirect_to users_path(current_user.id)
  end


  private

  # Check profile page access
  def show_user

    if !current_user?
      flash[:access_error] = "You cant get access to this page"
      redirect_to root_path
    end
  end

  def current_user?

    return false if current_user.blank?
    current_user.id == params[:id].to_i
  end

  # User params validation
  def user_params
    params.require(:user).permit(:name, :password, :avatar)
  end

  # Transaction params validation
  def transaction_params
    params.require(:transaction).permit(:balance, :take, :user_id)
  end

end
