class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  # Paperclip avatar validations
  has_attached_file :avatar, :styles => { :medium => "240x240>", :thumb => "80x80>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  # Associations
  has_many :transactions

  # Taken balance
  def self.balance_take(id)
    return self.totally(id).to_a.last[1].to_i
  end

  # Given balance
  def self.balance_give(id)
    return self.totally(id).first[1].to_i
  end

  private

  def self.totally(id)
    totally = Transaction.group(:user_id, :take)
                   .having(user_id: id)
                   .sum(:balance)
  end

end
