class SharedBank < ActiveRecord::Base
  validates :current, presence: true

  def self.update_balance(params)

    if self.take? params
      return self.take params[:balance].to_i
    else
      return self.give params[:balance].to_i
    end
  end

  private

  def self.take(balance)
    current = SharedBank.last.current

    if (updated_balance = current - balance) >= 0
      SharedBank.last
          .update_attribute(:current, updated_balance)

      return true
    else
      return false
    end
  end

  def self.give(balance)
    current = SharedBank.last.current

    SharedBank.last
        .update_attribute(:current, (current + balance))

    return true
  end

  def self.take?(params)
    params[:take] == "true"
  end

end
