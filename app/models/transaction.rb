class Transaction < ActiveRecord::Base
  validates :balance, presence: true
  validate :balance do |record|
    balance_validation record
  end


  private

  def balance_validation(record)

    if record.balance.to_i > SharedBank.last.current.to_i
      record.errors[:base] << "There are not enough money in Shared Bank, plese choose another balance"
    end
  end
end
