class CreateSharedBanks < ActiveRecord::Migration
  def change
    create_table :shared_banks do |t|
      t.integer :current

      t.timestamps null: false
    end
  end
end
