class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :user_id
      t.boolean :take
      t.integer :balance

      t.timestamps null: false
    end
  end
end
